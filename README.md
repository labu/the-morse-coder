# The Morse Coder

## Setting up the application

Clone the application from Bitbucket using:

    git clone git@bitbucket.org:labu/the-morse-coder.git

Once cloned, use the following to install the application's dependencies:

    npm install

## Running the application

To run the application locally use:

    npm run start

This will open the browser at http://localhost:4200 and run the application. Please note this application has been built to run in Google Chrome (currently version 76) and a screen size of 1920 x 1080

To build the application use:

    npm run build

You can also check for linting errors by using:

    npm run lint

## Overview of the Application

This application translate words into Morse Code. Visitors are drawn to the activation using the attractor screen. The visitor is prompted to continue where they can enter in their word or message of choice. The application will then translate their word into morse code.

### Styling

As each component is quite unique, the majority of styling is handled with the component. There is some base styling though, found in the src/sass/ folder:

- variables.scss contains all color values used throughout the app. By keeping these values in one file it is easy for us to change any value quickly
- base.scss contains base styling of HTML elements. Here we set our app size, main font colors and background images
- button.scss contains the button styling. As we use the same button style across the app, we store this globally and reuse it

### JavaScript Overview

Our app is separated into 3 views: Attractor, Translate and Result. These are found in the /src/app/views folder.

#### Attractor View

![Attractor Screen](docs/attractor.png)

The attractor screen contains a background image which fills the entire screen. Pressing anywhere on the screen will advance the user to the next screen (using the advance() function)

#### Translate View

![Translation Screen](docs/interaction.png)

The translate view allows the user to enter a message. The input will only expect a to z characters (lower or upper case), numbers and spaces. If this pattern is not matched, the button is disabled. A error message is also displayed once the form is dirty. Once a valid input is entered, the button can be pressed. This calls the submit() function which navigates to the result page, passing the message as a parameter.

The view also includes the timeout component (described below) that will reset the app after 30 seconds of no user interaction

#### Result View

![Result Screen](docs/results.png)

The results view shows the user their message in morse code as well as the telephony for each letter. The converting of each letter into morph code and telephony is handled via a pipe (described below). The morph pipe returns and array of either 'dot' and 'dash'. The value is used to select the correct image. Pressing the Try again button will reset to the Translate View.

The view also includes the timeout component (described below) that will reset the app after 30 seconds of no user interaction

#### Timeout Component

The timeout component will reset the application after 30 seconds when there is no user interaction. Event listeners are added for clicks and keypresses. When one of these events occur, the timer is restarted

#### Telephony Pipe

The telephony pipe translates a letter into the correct telephony word. The data contents is loaded from /src/assets/data/translations.json. We use the letter to check against this object. If a match is found we return the related telephony object. If there is no match we return an empty string

#### Morse Pipe

The morse pipe works the same as above. The additional task for the morse pipe is that we split the returned morse code into an array. The '-' and '.' are converted to 'dash' and 'dot' respectively to make it easier to obtain the correct image.
