import { Pipe, PipeTransform } from '@angular/core';
import * as DATA from '../../assets/data/translations.json';

@Pipe({
    name: 'telephony',
    pure: true
})
export class TelephonyConvertPipe implements PipeTransform {

    // Data from translations file
    file: any = DATA;
    // Store contents from file
    data: any = this.file.default;

    constructor() {}

    // Converts a letter to the telephony version
    transform(letter: string): string {
        const value = letter.toLowerCase();
        return this.data[value] ? this.data[value].telephony : '';
    }
}
