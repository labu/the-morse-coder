import { Pipe, PipeTransform } from '@angular/core';
import * as DATA from '../../assets/data/translations.json';

@Pipe({
    name: 'morse',
    pure: true
})
export class MorseConvertPipe implements PipeTransform {

    // Data from translations file
    file: any = DATA;
    // Store contents from file
    data: any = this.file.default;

    constructor() {}

    // Converts an array containing the morse code
    transform(letter: string): Array<string> {
        const value = letter.toLowerCase();
        const morsecode = this.data[value] ? this.data[value].morsecode : '';
        return morsecode.split('').map( (code: string) => code === '-' ? 'dash' : 'dot');
    }
}
