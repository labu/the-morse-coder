// Angular Modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// App Modules
import { AppRoutingModule } from './app-routing.module';

// Views
import { AppView } from './views/app/app.view';
import { AttractorView } from './views/attractor/attractor.view';
import { ResultView } from './views/result/result.view';
import { TranslateView } from './views/translate/translate.view';

// Components
import { TimeoutComponent } from './components/timeout/timeout.component';

// Pipes
import { MorseConvertPipe } from './pipes/morse.pipe';
import { TelephonyConvertPipe } from './pipes/telephony.pipe';

@NgModule({
    declarations: [
        AppView,
        AttractorView,
        ResultView,
        TranslateView,
        TimeoutComponent,
        MorseConvertPipe,
        TelephonyConvertPipe
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule
    ],
    providers: [
        MorseConvertPipe,
        TelephonyConvertPipe
    ],
    bootstrap: [AppView]
})
export class AppModule {}
