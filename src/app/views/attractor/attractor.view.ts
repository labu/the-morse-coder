import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-attractor',
    templateUrl: './attractor.view.html',
    styleUrls: ['./attractor.view.scss']
})
export class AttractorView {

    constructor(
        private router: Router
    ) {}

    // Breaks the attractor: takes the user to the translation page
    advance() {
        this.router.navigate(['translate']);
    }

}
