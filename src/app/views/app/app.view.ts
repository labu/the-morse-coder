import { Component } from '@angular/core';
import { animation } from '../../animations/route.animation';

@Component({
    selector: 'app-root',
    templateUrl: './app.view.html',
    styleUrls: ['./app.view.scss'],
    animations: [animation]
})
export class AppView {

    // Gets the new route and animates between the pages
    animateRoute(outlet: any) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    }
}
