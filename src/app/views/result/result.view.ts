import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-result',
    templateUrl: './result.view.html',
    styleUrls: ['./result.view.scss']
})
export class ResultView implements OnInit {

    // The message that the user has submitted
    message: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {}

    // User presses "Try again", return to the translate page
    startAgain() {
        this.router.navigate(['translate']);
    }

    // Get the users message from URL
    ngOnInit() {
        this.route.params.subscribe( (params: any) => {
            this.message = params.message;
        });
    }

}
