import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-translate',
    templateUrl: './translate.view.html',
    styleUrls: ['./translate.view.scss']
})
export class TranslateView implements OnInit {

    // The message the user has entered
    message: string;
    // Reference to the HTML input element
    @ViewChild('input') input: ElementRef;

    constructor(
        private router: Router
    ) {}

    // Navigates to result page, passes the user message
    submit(message: string) {
        this.router.navigate(['result', message]);
    }

    // Focuses on the input box on page start
    ngOnInit() {
        this.input.nativeElement.focus();
    }

}
