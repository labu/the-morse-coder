// Angular Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Views
import { AttractorView } from './views/attractor/attractor.view';
import { ResultView } from './views/result/result.view';
import { TranslateView } from './views/translate/translate.view';

const routes: Routes = [
    {
        'path': '',
        'component': AttractorView,
        'data': {'animation': 'Attractor'}
    },
    {
        'path': 'translate',
        'component': TranslateView,
        'data': {'animation': 'Translate'}
    },
    {
        'path': 'result/:message',
        'component': ResultView,
        'data': {'animation': 'Result'}
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
